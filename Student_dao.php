<?php

    include('select_values.php');
    class StudentDAO {
        private $pdo;

        public function __construct($pdo) {
            $this->pdo = $pdo;
        }

        public function getStudents() {

            try {
                $sql = "SELECT id, group_id, first_name, last_name, gender_id, birthday, status_id FROM students ORDER BY id";
            
                $result = $this->pdo->prepare($sql);
                $result->execute();
    
                $users = $result->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                return array('success' => false, 
                             'message' => $e->getMessage());
            }
            
            $count = count($users);

            global $groups;
            global $genders;

            foreach ($users as $key => $value) {
                $users[$key]['first_name'] = stripslashes(substr($value['first_name'], 1, -1));
                $users[$key]['last_name'] = stripslashes(substr($value['last_name'], 1, -1));
                $users[$key]['group_name'] = $groups[$value['group_id']];
                $users[$key]['gender_name'] = $genders[$value['gender_id']];
            }

            $responce = array(
                'success' => true,
                'count' => $count,
                'users' => $users
            );

            return $responce;
        }

        public function addStudent($user) {
            try {
                $sql = "INSERT INTO students ( group_id, first_name, last_name, gender_id, birthday, status_id) 
                VALUES ( :group_id, :first_name, :last_name, :gender_id, :birthday, :status_id )";
    
                $result = $this->pdo->prepare($sql);
                $result->execute(array(
                    ':group_id' => (int)$user['group_id'],
                    ':first_name' => $this->pdo->quote($user['first_name']),
                    ':last_name' => $this->pdo->quote($user['last_name']),
                    ':gender_id' => (int)$user['gender_id'],
                    ':birthday' => $user['birthday'],
                    ':status_id' => 1
                ));
            } catch (Exception $e) {
                return array('success' => false, 
                             'message' => $e->getMessage());
            }

            global $groups;
            global $genders;

            $user['id'] = $this->pdo->lastInsertId();
            $user['group_name'] = $groups[$user['group_id']];
            $user['gender_name'] = $genders[$user['gender_id']];


            return array('success' => true,
                         'user' => $user);
        }

        public function deleteStudent($id) {
            try {
                $sql = "DELETE FROM students WHERE id = :id";

                $result = $this->pdo->prepare($sql);
                $result->execute(array(
                    ':id' => (int)$id
                ));
            } catch (Exception $e) {
                return array('success' => false, 
                'message' => $e->getMessage());
            }
        
            return array('success' => true,
                         'id' => $id);        
        }
 
        public function updateStudent($user) {

            try {        
                $sql = "UPDATE students SET ";  
                foreach ($user as $key => $value) {
                    if ($key === 'id') {
                        continue;
                    }
                    $sql .= $key . " = :" . $key . ", ";
                }
                $sql = rtrim($sql, ", ");
                
                $sql .= " WHERE id = :id ;";

                $exec_arr = array();

                foreach ($user as $key => $value) {
                    if ($key === 'first_name' || $key === 'last_name') {
                        $slashed_value = $this->pdo->quote($value);
                        $exec_arr += array(':'.$key => $slashed_value);
                        continue;
                    } 
                    $exec_arr += array(':'.$key => $value);
                }

                $result = $this->pdo->prepare($sql);
                $result->execute($exec_arr);
            } catch (Exception $e) {
                return array('success' => false, 
                'message' => $e->getMessage());
            }

            global $groups;
            global $genders;
            
            if (array_key_exists('group_id', $user)) {
                $user['group_name'] = $groups[$user['group_id']];
            }

            if (array_key_exists('gender_id', $user)) {
                $user['gender_name'] = $genders[$user['gender_id']];
            }


            return array('success' => true,
                         'user' => $user);
        }
    }
?>