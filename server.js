const express = require('express');
const {createServer} = require('http');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
const { User, Chat } = require('./schemas.js');

const server = createServer(app);

const io = require("socket.io")(server, {
    cors: {
      origin: "http://localhost",
    },
  });

const PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: false}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

mongoose.connect("mongodb+srv://daniilvasilenko:s7GitIJvXTaDmT6z@cluster0.nfka4y6.mongodb.net/Students?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

app.post('/validate-log-in', async (req, res) => {
    const user = await User.findOne({username: req.body.username}).exec();
    console.log(user);
    if (user) {
        res.send({success: true, user: user});
    } else {
        res.send({success: false});
    }
});

app.post('/get-chat-titles', async (req, res) => {
    const chats = await Chat.find({participants: req.body.user_id}).select('title').exec();

    if (chats) {
        res.send({success: true, chats: chats});
    } else {
        res.send({success: false});
    }
});

app.get('/users', async (req, res) => {
    const users = await User.find().exec();
    
    if (users) {
        res.send({success: true, users: users});
    } else {
        res.send({success: false});
    }
});

app.post('/create-chat', async (req, res) => {
    
    const chat = new Chat({
        title: req.body.title,
        participants: req.body.participants,
        messages: [] 
    });

    const result = await chat.save();

    if (result) {
        res.send({success: true});
        for (const participant of req.body.participants) {
            if (currentClients.get(participant.toString()) !== undefined) {
                io.to(currentClients.get(participant.toString())).emit('receive-notif-new-chat', 'You have been added to a new chat!');
            }
        }
    } else {
        res.send({success: false});
    }
});

app.post('/get-chat', async (req, res) => {
    const chat = await Chat.findOne({_id: req.body.chat_id})
                           .populate({
                                path: 'participants',
                                model: 'User',
                                select: 'username'
                           })
                           .populate({
                                path: 'messages',
                                populate: {
                                    path: 'sender',
                                    model: 'User',
                                    select: 'username'
                                }
                           })
                           .exec();

    if (chat) {
        res.send({success: true, chat: chat});
    } else {
        res.send({success: false});
    }
});

server.listen(PORT, ()=> {
    console.log(`Server is running on port: ${PORT}`);
});

let currentClients = new Map();

io.on('connection', (socket) => {
    let socketId = socket.id;

    socket.on('user-info-sending', async (userId) => {
        currentClients.set(userId, socketId);

        console.log(`A user is identified with id ${userId} is connected to a socket with id ${socketId}`);
        
        const user = await User.findOne({_id: new mongoose.Types.ObjectId(userId)}).exec();

        const notif = user.username + " is now online !";

        socket.broadcast.emit('receive-notif-user-online', notif);
    });

    socket.on('send-message', async (chat_id, message, sender) => {
        console.log(`Received message: ${message} from chat: ${chat_id}, sender: ${sender}`);

        const chat = await Chat.findByIdAndUpdate(chat_id, {$push: {messages: {sender: sender, message: message}}}, { new: true })
                              .populate({
                                path: 'participants',
                                model: 'User',
                                select: 'username'
                              })
                              .select('messages')
                              .slice('messages', -1)
                              .populate({
                                path: 'messages',
                                populate: {
                                    path: 'sender',
                                    model: 'User',
                                    select: 'username'
                                }
                              })
                              .exec();

        const senderObject = chat.participants.find(part => part._id == sender);

        for (const participant of chat.participants) {
            if (currentClients.get(participant._id.toString()) !== undefined) {
                io.to(currentClients.get(participant._id.toString())).emit('receive-message', chat_id, chat.messages[0].message, senderObject.username);
            }
        }
    });

    socket.on('disconnect', () => {
        for (const [key, value] of currentClients.entries()) {
            if (value === socketId) {
                currentClients.delete(key);
                console.log(`A user disconnected with id: ${key}`);
                break;
            }  
        }  
    });
  });