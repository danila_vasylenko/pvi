<?php
    class Student {
        private $id;
        private $group_number;
        private $first_name;
        private $last_name;
        private $gender;
        private $birthday;

        public function __construct($group_number, $first_name, $last_name, $gender, $birthday) {
            $this->group_number = $group_number;
            $this->first_name = $first_name;
            $this->last_name = $last_name;
            $this->gender = $gender;
            $this->birthday = $birthday;
        }

        public function setId($id) {
            $this->id = $id;
        }
    }
?>