<?php
    header('Content-Type: application/json; charset=utf-8');

    require_once 'Student_dao.php';
    require_once 'validation.php';

    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['action'])) {
        echo json_encode(array('success' => false, 'message' => 'Wrong method invoked or no action.')); exit();
    }
    
    if (($_POST['action'] === 'CREATE' || $_POST['action'] === 'UPDATE')) {
        $responce = validate($_POST['user']);

        if ($responce['success'] !== true) {
            echo json_encode($responce);
            exit();
        }
    }

    $servername = "127.0.0.1";
    $db = "students";
    
    $pdo = new PDO("mysql:host=$servername;dbname=$db", "root", "");

    $studentDAO = new StudentDAO($pdo); 

    switch ($_POST['action']) {   
        case 'CREATE': 
            echo json_encode($studentDAO->addStudent($responce['user']));
            break;
        case 'READ':
            echo json_encode($studentDAO->getStudents());
            break;
        case 'UPDATE':
            echo json_encode($studentDAO->updateStudent($responce['user']));
            break;
        case 'DELETE':
            echo json_encode($studentDAO->deleteStudent($_POST['id']));
            break;
        default:
            echo json_encode(array('success' => false, 'message' => 'Wrong action invoked.'));
    }
?>