<?php
    function validate($data) {
        require_once('select_values.php');
        // print_r($data);
        $errors = array(); 
    
        if (array_key_exists('group_id', $data)) {
            if ($data['group_id'] === '0') {
                $errors['group_id'] = 'Group is required !';
            }
        }

        if (array_key_exists('first_name', $data)) {
            if (empty($data['first_name'])) {
                $errors['first_name'] = 'First name is required !';
            }
            elseif (!preg_match('/^[a-zA-Z]+$/', $data['first_name'])) {
                $errors['first_name'] = 'First name can only contain letters';
            }
        }
    
        if (array_key_exists('last_name', $data)) {
            if (empty($data['last_name'])) {
                $errors['last_name'] = 'Last name is required !';
            } 
            elseif (!preg_match('/^[a-zA-Z]+$/', $data['last_name'])) {
               $errors['last_name'] = 'Last name can only contain letters';
            }
        }

        if (array_key_exists('gender_id', $data)) {
            if ($data['gender_id'] === '0') {
                $errors['gender_id'] = 'Gender is required !';
            }
        }

        if (array_key_exists('birthday', $data)) {
            if (empty($data['birthday'])) {
                $errors['birthday'] = 'Birthday is required !';
            } elseif (strtotime($data['birthday']) < strtotime('1920-01-01') || strtotime($data['birthday']) > strtotime('2007-01-01')) {
                $errors['birthday'] = 'Birday must be from 1920 year up to 2007 !';
            }
        }

        $responce = array('success' => (empty($errors)) ? true : false, 'user' => $data);

        if (!empty($errors)) {
            $responce['message'] = 'Validation fault';
            $responce['errors'] = $errors;
        }

        return $responce;
    } 
?>