$(document).ready(function () {

    

    let studGG = {};
    getStudents();
    function getStudents() {
        $.ajax({
            url: '/Student/request.php',
            method: 'POST',
            dataType: 'json',
            data: { action: 'READ' },
            success: function(responce) {
                if (!responce.success) {
                    console.log("DB error: " + responce.message);
                    return;
                }

                const users = responce.users;

                for (var i = 0; i < users.length; i++) {     
                    addStudent(users[i]);
                }
            }
        });
    }

    function addStudent(data) {
        studGG[data.id] = {gender_id: data.gender_id,
                           group_id: data.group_id}; 
        $(".table-body").append("<tr>" +
        "<input type=\"hidden\" value=\"" + data.id + "\">" +
        "<td><input type=\"checkbox\" class=\"form-check-input\"></td>" +
        "<td><span class=\"table-group\">" + data.group_name + "</span></td>" +
        "<td><span class=\"table-name\">" + data.first_name + " " + data.last_name + "</span></td>" +
        "<td><span class=\"table-gender\">" +  data.gender_name  + "</span></td>" +
        "<td><span class=\"table-birthday\">" + convertFromISO(data.birthday) + "</span></td>" +
        (data.status_id === "0" ? "<td><div class=\"status\"></div></td>" : "<td><div class=\"status active\"></div></td>") +
        "<td><div class=\"table-options d-flex flex-row\">" +
        "<div class=\"btn edit-button user-options border-0 py-0 px-2\" data-bs-toggle=\"modal\" data-bs-target=\"#modal-add-edit-form\" data-id=\"" + data.id + "\"><i class=\"bi bi-pencil\"></i></div>" +
        "<div class=\"btn delete-button border-0 py-0 px-2\" data-bs-target=\"#deleteModal\" data-bs-toggle=\"modal\" ><i class=\"bi bi-x-lg\"></i></div></div></td>" +
        "</tr>");
        // console.log(studGG);
    }

    function studentToEdit(tr) {
        let student = {};

        student.id = tr.find('input[type=hidden]').val();
        student.group_id = studGG[student.id].group_id;
        //$('.group-select option:contains("' + tr.find(".table-group").text() + '")').val();
        
        let name_array = tr.find(".table-name").text().split(" ");

        student.first_name = name_array[0];
        student.last_name = name_array[1];
        student.gender_id = studGG[student.id].gender_id;
        // $('.gender-select option:contains("' + tr.find(".table-gender").text() + '")').val();
        student.birthday = convertToISO(tr.find(".table-birthday").text());

        return student;
    }

    $(document).on("click", ".delete-button", function () {
        var user_id = $(this).closest('tr').find('input[type="hidden"]').val();
        var rowToDelete = $(this).closest('tr');

        $(".delete-modal-body").text("Are you sure, that you want to remove user "
            + $(this).parents("tr").find("td:eq(2)").text() + "?");

        $(".delete-modal-footer").on("click", "#submitDelete", function () {
            $.ajax({
                url: '/Student/request.php',
                method: 'POST',
                dataType: 'json',
                data: {id: user_id,
                       action: 'DELETE'},
                success: function(responce) {
                    console.log(responce);

                    if (responce.success === true) {
                        rowToDelete.remove();
                    } else {
                        console.log(responce.message);
                    }
                }
            });
        });
    });

    $(document).on("click", ".user-options", function () {
        let dataId = $(this).attr("data-id");
        let tr = $(this).closest("tr");

        if (dataId === "") {
            $("#modalTitle").text("Add student");
            $(".group-select").val("0");
            $(".gender-select").val("0");
        } else {
            $("#modalTitle").text("Edit student");
            
            let user = studentToEdit(tr);

            $(".modal-data-id").val(user.id);
            $(".group-select").val(user.group_id);
            $(".first-name-field").val(user.first_name);
            $(".last-name-field").val(user.last_name);
            $(".gender-select").val(user.gender_id);
            $(".date-select").val(user.birthday);
        }
    });

    function convertFromISO(birthdate) {
        return birthdate.slice(8, 10) + "-" + birthdate.slice(5, 7) + "-" + birthdate.slice(0, 4);
    }

    function convertToISO(birthdate) {
        return birthdate.slice(6) + "-" + birthdate.slice(3, 5) + "-" + birthdate.slice(0, 2);
    }

    function clearModal() {
        $(".group-select").removeClass('is-valid is-invalid');
        $(".first-name-field").removeClass('is-valid is-invalid');
        $(".last-name-field").removeClass('is-valid is-invalid');
        $(".gender-select").removeClass('is-valid is-invalid');
        $(".date-select").removeClass('is-valid is-invalid');


        $("#group-error").prop('hidden', true);
        $("#first-name-error").prop('hidden', true);
        $("#last-name-error").prop('hidden', true);
        $("#gender-error").prop('hidden', true);
        $("#birthday-error").prop('hidden', true);
    }

    $("#modal-add-edit-form").submit(function (event) {
        event.preventDefault();
        clearModal();

        let sendObject = {
            action: null,
            user: {}
        };

        let modalData = {
            id:  $(".modal-data-id").val(),
            group_id: $(".group-select").val(),
            
            first_name: $(".first-name-field").val(),
            last_name: $(".last-name-field").val(),
            gender_id: $(".gender-select").val(),
            
            birthday: $(".date-select").val()
        };

        if ($(".modal-data-id").val() === "") {
            sendObject.action = 'CREATE';
            sendObject.user = modalData;
        } else {
            sendObject.action = 'UPDATE';

            let tr = $(".table-body input[type=hidden][value='" + $(".modal-data-id").val() + "']").closest('tr');
            let userOld = studentToEdit(tr);
            let isEdited;

            $.each(userOld, function(field, value) {
                if (modalData[field] !== value) {
                    sendObject.user[field] = modalData[field];
                    if (field === 'group_id') {
                        sendObject.user['group_name'] = modalData.group_name;
                    }
                    if (field === 'gender_id') {
                        sendObject.user['gender_name'] = modalData.gender_name;
                    }
                    isEdited = true;
                }
            });
            if (!isEdited) {
                alert("You didn`t edit nothing!");
                return;
            }

            sendObject.user['id'] = userOld.id;
        }

        $.ajax({
            url: '/Student/request.php',
            method: 'POST',
            dataType: 'json',
            data: sendObject,
            success: function (responce) {
                console.log(responce);

                if (!responce.success && responce.message === 'Validation fault') {
                    if (responce.errors.group_id) {
                        $(".group-select").addClass('is-invalid');
                        $("#group-error").text(responce.errors.group_id);
                        $("#group-error").prop('hidden', false);
                    } else {
                        $(".group-select").addClass('is-valid');
                    }

                    if (responce.errors.first_name) {
                        $(".first-name-field").addClass('is-invalid');
                        $("#first-name-error").text(responce.errors.first_name);
                        $("#first-name-error").prop('hidden', false);
                    } else {
                        $(".first-name-field").addClass('is-valid');
                    }

                    if (responce.errors.last_name) {
                        $(".last-name-field").addClass('is-invalid');
                        $("#last-name-error").text(responce.errors.last_name);
                        $("#last-name-error").prop('hidden', false);
                    } else {
                        $(".last-name-field").addClass('is-valid');
                    }

                    if (responce.errors.gender_id) {
                        $(".gender-select").addClass('is-invalid');
                        $("#gender-error").text(responce.errors.gender_id);
                        $("#gender-error").prop('hidden', false);
                    } else {
                        $(".gender-select").addClass('is-valid');
                    }

                    if (responce.errors.birthday) {
                        $(".date-select").addClass('is-invalid');
                        $("#birthday-error").text(responce.errors.birthday);
                        $("#birthday-error").prop('hidden', false);
                    } else {
                        $(".date-select").addClass('is-valid');
                    }
                    return;
                }

                if (!responce.success) {
                    console.log(responce.message);
                    return;
                }

                if (sendObject.action === 'CREATE') {
                    addStudent(responce.user);
                    $("#modal-add-edit-form").modal("hide");
                } 

                if (sendObject.action === 'UPDATE') {
                    let fieldToHtml = {
                        group_name: ".table-group",
                        first_name: ".table-name",
                        last_name: ".table-name",
                        gender_name: ".table-gender",
                        birthday: ".table-birthday"
                    };

                    let row = $(".table-body").find("input[value='" + Number(responce.user.id) + "']").closest('tr');
                    $('.table-body').append(row);

                    responce.user.group_name = $(".group-select [value='"+$(".group-select").val()+"']").text();
                    responce.user.gender_name = $(".gender-select [value='"+$(".gender-select").val()+"']").text();

                    $.each(responce.user, function(key, value) {
                        if (key === 'birthday') {
                            row.find(fieldToHtml[key]).text(convertFromISO(value));
                        }
                        if (key === 'first_name' || key === 'last_name') {
                            console.log(fieldToHtml[key]);
                            let name_arr = row.find(fieldToHtml[key]).text().split(" ");
                            console.log(name_arr[0], name_arr[1]);
                            if (key === 'first_name') {
                                row.find(fieldToHtml[key]).text(value + " " + name_arr[1]);
                            } else if (key === 'last_name') {
                                row.find(fieldToHtml[key]).text(name_arr[0] + " " + value);
                            }
                        } else {
                            row.find(fieldToHtml[key]).text(value);
                        }
                    });
                    $("#modal-add-edit-form").modal("hide");
                }
            }
        });
    });

    $("#modal-add-edit-form").on("hidden.bs.modal", function (event) {
        event.preventDefault();
        clearModal();
        $(".modal-data-id").val = "";
        $(".group-select").val("0");
        $(".first-name-field").val("");
        $(".last-name-field").val("");
        $(".gender-select").val("0");
        $(".date-select").val("");
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    if (window.location.href === 'http://localhost/student/chat.php') {
        $("#nav-chat").addClass("active");
        $("#offcanvas-log-in").offcanvas('toggle');
    }

    if (window.location.href === 'http://localhost/student/index.php' || window.location.href === 'http://localhost/student/') {
        $("#nav-table").addClass("active");
    }

    

    $('#create-chat').on('click', function() {
        $.ajax({
            url: 'http://localhost:3000/users',
            method: 'get',
            success: function(res) {
                if (res.success) {
                    for (const user of res.users) {
                        if (user._id != clientObject.id) {
                            $('#modal-users').append("<li>"+
                            "<input class=\"form-check-input\" type=\"checkbox\">"
                            +"<input type=\"hidden\" value=\""+user._id+"\">"
                            +"<span class=\"ms-2\">"+user.username+"</span>"
                            +"</li>");
                        }
                    }
                    
                }
            }
        })
        
    });

    $('#modal-create-chat').submit(function(event) {
        event.preventDefault();
        const title = $(".title-field").val();

        const list_items = $("ul li:has(:checkbox:checked)");

        const participants = [];

        participants.push(clientObject.id);

        for (const li of list_items) {
            participants.push($(li).find('input[type="hidden"]').val());
        }

        console.log(participants);

        $.ajax({
            url: 'http://localhost:3000/create-chat',
            method: 'POST',
            traditional: true,
            data: {title: title, participants: participants},
            success: function(res) {
                console.log(res);
            }
        })
        $('#modal-create-chat').modal('hide');
    });

    class Client {
        
        constructor(username, id) {
            this.username = username;
            this.id = id;
            this.socket = io('http://localhost:3000', { autoConnect: true });
            
            this.socket.emit('user-info-sending', this.id);

            this.getChatsList();

            this.socket.on('receive-message', (chat_id, message, sender) => {
                if ($("#concrete-chat-name").find('h5').attr('data-id') == chat_id) {
                    const messageContainer = $('#concrete-chat-messsage-box');
                    if (sender == clientObject.username) {
                        $("#concrete-chat-message-box").append(
                            "<li class=\"row list-group-item mt-2 mx-0 d-flex justify-content-end\">" + 
                                "<div class=\"col-9 border rounded d-flex align-items-center my-message\" style=\"width: auto; height: auto;\">" +
                                    message +
                                "</div>" +
                                "<div class=\"col-1 me-1 text-center d-flex flex-column justify-content-center align-items-center\">" +
                                    "<i class=\"bi bi-person-circle mx-md-2\"></i>" +
                                    "<span>Me</span>" +
                                "</div>" +
                            "</li>"
                    );
                    } else {
                        $("#concrete-chat-message-box").append(
                            "<li class=\"row list-group-item mt-2 mx-0 d-flex\">" +
                                "<div class=\"col-1 ms-1 px-1 text-center d-flex flex-column justify-content-center align-items-center\" style=\"width: fit-content\">" +
                                    "<i class=\"bi bi-person-circle mx-md-2\"></i>" +
                                    "<span>" +
                                        sender +
                                    "</span>" +	
                                "</div>" +
                                    "<div class=\"col-9 border rounded d-flex align-items-center\" style=\"width: auto; height: auto;\">" +
                                        message +
                                    "</div>" +
                            "</li>"
                        );
                    }
                }
            });

            this.socket.on('receive-notif-new-chat', (notif) => {
                $('.toast-body').text(notif);
                $('#simpleNotification').toast('show');

                $('.chat-list-member').remove();
                this.getChatsList();
            });

            this.socket.on('receive-notif-user-online', (notif) => {
                $('.toast-body').text(notif);
                $('#simpleNotification').toast('show');
            });
        }

        getChatsList() {
            $.ajax({
                url: 'http://localhost:3000/get-chat-titles',
                method: 'POST',
                data: {user_id: this.id},
                success: function(res) {
                    if (res.success) {
                        for (const chat of res.chats) {
                            $("#chat-room-list").append(
                                "<div class=\"border mx-md-3 my-md-2 rounded chat-list-member\">" +
                                    "<i class=\"bi bi-person-circle mx-md-2\"></i>" +
                                    "<input value=\""+chat._id+"\" hidden>"+
                                    "<span>" + chat.title + "</span>"+
                                "</div>"
                            );
                        }
                    } else {
                        console.log("There is not chats with this user yet");
                    }
                }

            });   
        }

        getChat(chat_id) {
            const current_username = this.username;

            $.ajax({
                url: 'http://localhost:3000/get-chat',
                method: 'POST',
                data: {chat_id: chat_id},
                success: function(res) {
                    $("#concrete-chat-name").html(
                        "<h5 class=\"mx-2 my-1\" style=\"font-size: 32px;\" data-id=\"" + res.chat._id + "\">" + 
							"Chat room " + res.chat.title +
						"</h5>"
                    );

                    console.log(res.chat.participants);

                    for (const participant of res.chat.participants) {
                        console.log(participant);
                        $("#concrete-chat-members").append(
                            "<span class=\"me-2\" data-id=\""+participant._id+"\">"+participant.username+"</span>" 
                        );
                    }

                    for (const message of res.chat.messages) {
                        if (message.sender.username === current_username) {
                            $("#concrete-chat-message-box").append(
                                "<li class=\"row list-group-item mt-2 mx-0 d-flex justify-content-end\">" + 
                                    "<div class=\"col-9 border rounded d-flex align-items-center my-message\" style=\"width: auto; height: auto;\">" +
                                        message.message +
                                    "</div>" +
                                    "<div class=\"col-1 me-1 text-center d-flex flex-column justify-content-center align-items-center\">" +
                                        "<i class=\"bi bi-person-circle mx-md-2\"></i>" +
                                        "<span>Me</span>" +
                                    "</div>" +
                                "</li>"
                        );
                        } else {
                            $("#concrete-chat-message-box").append(
                                "<li class=\"row list-group-item mt-2 mx-0 d-flex\">" +
                                    "<div class=\"col-1 ms-1 px-1 text-center d-flex flex-column justify-content-center align-items-center\" style=\"width: fit-content\">" +
                                        "<i class=\"bi bi-person-circle mx-md-2\"></i>" +
                                        "<span>" +
                                            message.sender.username +
                                        "</span>" +	
                                    "</div>" +
                                        "<div class=\"col-9 border rounded d-flex align-items-center\" style=\"width: auto; height: auto;\">" +
                                            message.message +
                                        "</div>" +
                                "</li>"
                            );
                    
                        }
                    }
                }
            })

            
        }

        
    }

    let clientObject;

    $("#log-in-button").on('click', () => {
        $("#log-in-input").removeClass('is-valid is-invalid');
        $("#log-in-error").removeClass('d-block');
        $("#log-in-error").addClass('d-none');

        let usernameInput = $("#log-in-input").val();

        $.ajax({
            url: 'http://localhost:3000/validate-log-in',
            method: 'POST',
            data: {username: usernameInput},
            success: function(res) {
                if (res.success) {
                    $("#log-in-input").addClass('is-valid');
                    $("#offcanvas-log-in").offcanvas('toggle');
                    clientObject = new Client(res.user.username, res.user._id);
                    console.log(clientObject);
                    $("#profile-name").text(clientObject.username);
                        
                    // clientObject.getChatsList();
                } else {
                    $("#log-in-input").addClass('is-invalid');
                    $("#log-in-error").text("Invalid username.");
                    $("#log-in-error").removeClass('d-none');
                    $("#log-in-error").addClass('d-block');
                }
            }
        });
    });

    function clearMessageBox() {
        $("#concrete-chat-name").html(
            "<h5 class=\"mx-2 my-1\" style=\"font-size: 32px;\">" +
                "Chat room" + 
            "</h5>");
        $("#concrete-chat-members").text("");
        $("#concrete-chat-message-box").text("");
    }
    
    $(document).on('click', ".chat-list-member", function() {
        clearMessageBox();
        clientObject.getChat($(this).find("input").val());
    });
    
    $(document).on('click', "#send-message-button", function () {
        if ($("#send-message-input").val() === "") {
            console.log("You can`t send an empty message");
            return;
        }

        const receiver = [];

        const spans = $("#concrete-chat-members").find('span');
        for (let i = 0; i < spans.length; i++) {
            if ($(spans[i]).attr('data-id') != clientObject.id) {
                receiver.push($(spans[i]).attr('data-id'));
            }
        }

        clientObject.socket.emit("send-message", $("#concrete-chat-name").find('h5').attr('data-id'), $("#send-message-input").val(), clientObject.id);
        $("#send-message-input").val("");
    });
});




