const {Schema, model} = require('mongoose');

const UserSchema = new Schema({
    username: String
});

const MessageSchema = new Schema({
    sender: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    message: String
});

const ChatSchema = new Schema({
    title: String,
    participants: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    messages: [MessageSchema]
});

const User = model('User', UserSchema, 'Users');

const Chat = model('Chat', ChatSchema, 'Chats');

module.exports = {User, Chat};