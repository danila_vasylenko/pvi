<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD"
          crossorigin="anonymous">

    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">

    <link rel="stylesheet"
          href="Style/style.css">

    <link rel="manifest" href="manifest.json">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
            crossorigin="anonymous">
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

    <script src="https://cdn.socket.io/4.3.2/socket.io.min.js"></script>

    <script src="Script/script.js"></script>

    

    <script>
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('/Student/sw.js').then(function(registration) {
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                }, function(err) {
                    console.log('ServiceWorker registration failed: ', err);
                });
            });
        }
    </script>



    <title>Students</title>

</head>
<body>
    <header class="d-flex flex-wrap justify-content-center pt-1 py-sm-3 mb-4 border-bottom">
        <button class="btn d-md-none pb-2 border-0" id="toggle" type="button"
            data-bs-toggle="offcanvas" data-bs-target="#offcanvasResponsive"
            aria-controls="offcanvasResponsive">
                <i class="bi bi-list" style="font-size: 24px"></i>
            </button>

        <form class="offcanvas-md offcanvas-top" tabindex="-1" id="offcanvasResponsive" aria-labelledby="offcanvasResponsiveLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasResponsiveLabel">IES</h5>
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" data-bs-target="#offcanvasResponsive" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="nav flex-column d-md-none" id="top-bar">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#">
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">
                            Students
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Chat
                        </a>
                    </li>
                </ul>
            </div>
        </form>

        <a href="#" id="logo" class="d-flex align-items-center pb-1 pb-md-0 mb-md-0 me-md-auto text-dark text-decoration-none">
            <span class="fs-4 ps-md-3">IES</span>
        </a>

        <i class="bi bi-bell mt-1 mt-md-0 mx-3 position-relative"  data-bs-toggle="dropdown" id="notifications" style="font-size: 24px">
            <span id="notif-exist" class="position-absolute translate-middle border-0 rounded-circle"></span>
            <form class="dropdown-menu p-1 notify-form">
                <ul class="d-flex flex-column ps-0 mb-0" id="notify-body" type="none">
                    <li class="single-notif">
                        <div class="messager">
                            <i class="bi bi-person-circle" style="font-size: 24px"></i>
                            <span>Admin</span>
                        </div>
                        <span class="notif-message"></span>
                    </li>
                    <li class="single-notif">
                        <div class="messager">
                            <i class="bi bi-person-circle" style="font-size: 24px"></i>
                            <span>Teacher</span>
                        </div>
                        <span class="notif-message"></span>
                    </li>
                    <li class="single-notif">
                        <div class="messager">
                            <i class="bi bi-person-circle" style="font-size: 24px"></i>
                            <span>Dmytro</span>
                        </div>
                        <span class="notif-message"></span>
                    </li>
                </ul>
            </form>
        </i>

        <div class="mt-1 mt-md-0 pe-3 dropdown text-end" id="profile">
            <a href="#"  class="d-flex align-items-center link-dark text-decoration-none dropdown-toggle no-arrow" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="Images/photo.png" alt="profile-photo" width="32" height="32" class="rounded-circle">
                <span class="ps-2 ps-md-1"  id="profile-name">Anatolii Kochan</span>
            </a>

            <ul class="dropdown-menu text-small">
                <li><a class="dropdown-item" href="#">Profile</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Log out</a></li>
            </ul>
        </div>
    </header>


    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-0 col-md-3 col-lg-2 d-md-block ps-4 pe-0 pt-1 sidebar collapse">
                <div class="position-sticky sidebar-sticky">
                    <ul class="nav flex-column" id="side-list">
                        <li class="nav-item">
                            <a class="nav-link" href="#" title="#">
                                Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="nav-table" href="/student/index.php">
                                Students
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="nav-chat" href="/student/chat.php">
                                Chat
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main class="col-sm-12 col-md-9 ms-sm-auto col-lg-10 pe-md-4" id="main-chat">
				<h1 class="display-5">
					Messages
				</h1>
				<div class="row chat-container">
					<div class="h-100 col-md-3 mt-md-3  border rounded d-flex flex-column" id="chat-room-list">
						<div class="row">
                            <h5 class="ms-2 col-5 my-2">
							    Chat room
						    </h5>
                            <button type='button' id="create-chat" data-bs-target="#modal-create-chat" data-bs-toggle="modal"  class="col-6 d-flex justify-content-end btn border-0 border-secondary my-2 me-3 py-0"> 
                                <i class="bi bi-plus-lg"></i> 
                                New chat
							</button>
                        </div>

					</div>
					<div class="col-md-1"></div>
					<div class="h-100 col-md-8 mt-md-3 border rounded d-flex flex-column" id="concrete-chat">
						<div id="concrete-chat-name">
							<h5 class="mx-2 my-1" style="font-size: 32px;">
								Chat room
							</h5>
						</div>

						<div class="d-flex flex-column">
							<h5 class="mx-2 my-1">
							    Members
							</h5>
							<div class="flex-row mx-2 my-1" id="concrete-chat-members">
							</div>
						</div>
						<h5 class="mx-2 my-1">
							Messages
						</h5>
						<div class="d-flex mx-2 my-1 border rounded overflow-auto" id="message-box" style="height: 420px;">
							<ul class="list-unstyled flex-fill" id="concrete-chat-message-box">
							</ul>
						</div>
						<div class="border justify-content-between rounded row mx-2 my-1 no-wrap">
							<input class="col-9  border rounded my-2 ms-3 px-1 py-0" type="text" id="send-message-input">
							<button type='button' id="send-message-button" class="col-2 btn border border-secondary my-2 me-3 py-0">
								<i class="bi bi-send"></i>
							</button>
						</div>
					</div>
					
					</div>
				</div>

			</main>
        </div>
    </div>
    
    <form class="offcanvas offcanvas-top" tabindex="-1" id="offcanvas-log-in" aria-labelledby="offcanvasResponsiveLabel">
		<div class="offcanvas-header">
			<h5 class="offcanvas-title" id="offcanvasLabel">IES</h5>
		</div>
		<div class="offcanvas-body d-flex flex-column" >
			<h2 class="justify-self-start" style="width: 248px;">
				Log in
			</h2>
			
			<div class="row g-0">
				<input id="log-in-input" class="col-9 input-group-text form-control" style="width: 75%;">
				<div class="col-1"></div>
				<button type='button' id="log-in-button" class="col-2 btn border border-secondary">
					<i class="bi bi-box-arrow-in-right"></i>
				</button>
				
			</div>
			<div id="log-in-error" class="invalid-feedback d-flex" style="width: 248px;" hidden>
				
			</div>
		</div>
	</form>

    
    <div class="toast-container position-fixed top-0 start-50 translate-middle-x p-3">
        <div id="simpleNotification" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="me-auto">Chat</strong>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
            </div>
        </div>
    </div>

    <form class="modal fade" id="modal-create-chat" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-2" id="modalTitle" >Create new chat</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <p class="fs-4 fw-normal">Title</p>
                        <div class="mb-3">
                            <input class="col-12 input-group-text title-field form-control" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <p class=" fs-4 fw-normal">Members</p>
                        <div class=" mb-3 overflow-auto" style="height: 100px;">
                            <ul class="list-unstyled flex-fill" id="modal-users">
                            </ul>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary modal-submit" name="submit" value="Submit">
                </div>
            </div>
        </div>
    </form>

</body>
</html>