const mongoose = require('mongoose');
const {User, Chat} = require('./schemas.js');

mongoose.connect("mongodb+srv://to0sya:I4ylx6biuyN8UJr2@studentscluster.n8epxod.mongodb.net/StudentChat?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

const database = mongoose.connection;

database.on('error', console.error.bind(console, 'connection error:'));

database.once('open', function() {
    const chat = new Chat({
        title: 'Friends',
        participants: ['647df41b133c4c3567011a0c','647df435133c4c3567011a0e','647df442133c4c3567011a10'],
        messages: [
            {
                sender: '647df41b133c4c3567011a0c',
                message: 'Hey'
            },
            {
                sender: '647df442133c4c3567011a10',
                message: 'Helloooo'
            },
            {
                sender: '647df435133c4c3567011a0e',
                message: 'What`s up ;d'
            }
        ]
    });
    chat.save()
        .then(chat => console.log(chat.title + " saved to collection."))
        .catch(err => console.error(err));
});