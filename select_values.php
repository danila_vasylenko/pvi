<?php
    $genders = array(0 => 'Select',
                     1 => 'Male', 
                     2 => 'Female',
                     3 => 'Other');

    $groups = array(0 => 'Select',
                    1 => 'PE-21', 
                    2 => 'PE-22', 
                    3 => 'PE-23',
                    4 => 'PE-24',
                    5 => 'PE-25',
                    6 => 'PE-26');
?>